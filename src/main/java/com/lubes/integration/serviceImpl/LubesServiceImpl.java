package com.lubes.integration.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acceval.integration.ws.ConditionField;
import com.acceval.integration.ws.ConditionRecordsInMessage;
import com.acceval.integration.ws.ConditionRecordsWebService;
import com.acceval.integration.ws.ConditionRecordsWebServicePortType;
import com.acceval.integration.ws.ConditionValue;
import com.acceval.integration.ws.Item;
import com.acceval.integration.ws.SendConditionRecords;
import com.acceval.integration.ws.SendConditionRecordsResponse;
import com.acceval.integration.ws.WebServiceStatusMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lubes.integration.entity.EtTransrate;
import com.lubes.integration.repo.LubesRepo;
import com.lubes.integration.service.LubesService;
import com.lubes.integration.util.IoclDateUtil;

@Service
public class LubesServiceImpl implements LubesService {
	private static final Logger logger = LogManager.getLogger(LubesServiceImpl.class);

	private ConditionRecordsWebServicePortType webServicePort;

	// Initialize the service endpoint
	public LubesServiceImpl() {
		ConditionRecordsWebService conditionWebService = new ConditionRecordsWebService();
		webServicePort = conditionWebService.getConditionRecordsWebServicePort();
	}

	@Autowired
	LubesRepo transRepo;

	@Autowired
	IoclDateUtil ioclUtil;

	@Override
	public List<EtTransrate> getAllTransrate() {
		String shipping_condition = "BY_LAND";
		List<String> customrByPlant = null;
		List<String> tuNumberList = null;
		List<String> capClusterList = null;
		ConditionField conditionField = new ConditionField();
		ConditionRecordsInMessage inMsg = new ConditionRecordsInMessage();
		ConditionValue conditionValue = new ConditionValue();
		// SendConditionRecords sendConditionRecords = new SendConditionRecords();
		WebServiceStatusMessage response = null;

		Item item = new Item();

		conditionField.setCode("DELIVERY_MODE");
		conditionField.setValue(shipping_condition);

		item.setValidFrom(new JAXBElement<>(new QName("validFrom"), String.class, "08-01-2024 12:00:00"));
		item.setValidFrom(new JAXBElement<>(new QName("validTo"), String.class, "01-01-2024 12:00:00"));
		try {
			List<Integer> materialCodeList = transRepo.getAllMaterialCodes();

			List<String> plantIDList = transRepo.getAllPlant();
			if (plantIDList != null)
				customrByPlant = transRepo.getCustomerByPlant(plantIDList);
			if (customrByPlant != null && plantIDList != null) {
				tuNumberList = transRepo.getTuNumberByPlant(customrByPlant, plantIDList);
				capClusterList = transRepo.getCapClusterByPlant().stream().map(cluster -> {
					if ("w1".equalsIgnoreCase(cluster)) {
						return "T3000";
					} else if ("w2".equalsIgnoreCase(cluster)) {
						return "T3002";
					} else {
						return cluster;
					}
				}).collect(Collectors.toList());
			}

			for (Integer materialCode : materialCodeList) {
				conditionField.setCode("PRODUCT");
				conditionField.setValue(String.valueOf(materialCode));

				conditionField.setCode("PLANT");
				conditionField.setCode(String.valueOf(plantIDList));

				conditionField.setCode("CUSTOMER");
				conditionField.setCode(String.valueOf(customrByPlant));

				if (!String.valueOf(materialCode).endsWith("000")) {
					conditionField.setCode("DELIVERT_MODE");
					conditionField.setCode(String.valueOf(tuNumberList));

				} else if (String.valueOf(materialCode).endsWith("000")) {
					conditionField.setCode("DELIVERT_MODE");
					conditionField.setCode(String.valueOf(capClusterList));
				}
				item.getConditionFields().add(conditionField);
			}
			// condition value
			item.getConditionValues().add(conditionValue);

			// bring every thing toget her and sent to st
			inMsg.getItems().add(item);
			ObjectMapper mapper = new ObjectMapper();

			logger.info("data set after fetching :: " + mapper.writeValueAsString(inMsg));
//			if (inMsg != null) {
//				 response = webServicePort.sendConditionRecords(inMsg);
//				if (response.getStatus().equalsIgnoreCase("true")) {
//					logger.info("Data saved to smarttradzt successfully");
//				}
//			}

		} catch (Exception e) {
			logger.error("unable to send plant to customer transactional data", e);
		}

		// String materialCode =
		return null;
	}


	@Override
	public List<EtTransrate> processDataForPlant2CFA() {
		String shipping_condition = "BY_LAND";
		List<String> customrByPlant = null;
		List<String> tuNumberList = null;
		List<String> capClusterList = null;
		ConditionField conditionField = new ConditionField();
		ConditionRecordsInMessage inMsg = new ConditionRecordsInMessage();
		ConditionValue conditionValue = new ConditionValue();

		WebServiceStatusMessage response = null;

		Item item = new Item();
		item.setCode("DELIVERY_CHARGE_PLANT_TO_CFA");
		List<ConditionField> conditionFields = new ArrayList<>();

		ConditionField conditionField1 = new ConditionField();
		conditionField1.setCode("DELIVERY_MODE");
		conditionField1.setValue(shipping_condition);
		conditionFields.add(conditionField1);



		item.setValidFrom(new JAXBElement<>(new QName("validFrom"), String.class, "08-01-2024 12:00:00"));
		item.setValidFrom(new JAXBElement<>(new QName("validTo"), String.class, "01-01-2024 12:00:00"));
		try {
			List<Integer> materialCodeList = transRepo.getAllMaterialCodes();

			List<String> plantIDList = transRepo.getAllPlant();
			if (plantIDList != null)
				customrByPlant = transRepo.getCustomerByPlant(plantIDList);
			if (customrByPlant != null && plantIDList != null) {
				tuNumberList = transRepo.getTuNumberByPlant(customrByPlant, plantIDList);
				capClusterList = transRepo.getCapClusterByPlant().stream().map(cluster -> {
					if ("w1".equalsIgnoreCase(cluster)) {
						return "T3000";
					} else if ("w2".equalsIgnoreCase(cluster)) {
						return "T3002";
					} else {
						return cluster;
					}
				}).collect(Collectors.toList());
			}

			for (Integer materialCode : materialCodeList) {
				ConditionField conditionField2 = new ConditionField();
				conditionField2.setCode("PRODUCT");
				conditionField2.setValue(String.valueOf(materialCode));
				conditionFields.add(conditionField2);

				ConditionField conditionField3 = new ConditionField();
				conditionField3.setCode("PLANT");
				conditionField3.setCode(String.valueOf(plantIDList));
				conditionFields.add(conditionField3);

				ConditionField conditionField4 = new ConditionField();
				conditionField4.setCode("CUSTOMER");
				conditionField4.setCode(String.valueOf(customrByPlant));
				conditionFields.add(conditionField4);


				if (!String.valueOf(materialCode).endsWith("000")) {
					ConditionField conditionField5 = new ConditionField();
					conditionField5.setCode("DELIVERT_MODE");
					conditionField5.setCode(String.valueOf(tuNumberList));
					conditionFields.add(conditionField5);


				} else if (String.valueOf(materialCode).endsWith("000")) {
					conditionField.setCode("DELIVERT_MODE");
					conditionField.setCode(String.valueOf(capClusterList));
				}
				item.getConditionFields().addAll(conditionFields);

				//SHIPPING_CONDITION
				//STORAGE_LOCATION
			}
			// condition value
			item.getConditionValues().add(conditionValue);

			// bring every thing toget her and sent to st
			inMsg.getItems().add(item);
			ObjectMapper mapper = new ObjectMapper();

			logger.info("data set after fetching :: " + mapper.writeValueAsString(inMsg));
//			if (inMsg != null) {
//				 response = webServicePort.sendConditionRecords(inMsg);
//				if (response.getStatus().equalsIgnoreCase("true")) {
//					logger.info("Data saved to smarttradzt successfully");
//				}
//			}

		} catch (Exception e) {
			logger.error("unable to send plant to cfa transactional data", e);
		}

		// String materialCode =
		return null;
	}
}
