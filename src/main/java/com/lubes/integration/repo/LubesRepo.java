package com.lubes.integration.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lubes.integration.entity.EtTransrate;

@Repository
public interface LubesRepo extends JpaRepository<EtTransrate, Integer> {

	@Query(value = "select mt.material_code from MaterialTable as mt")
	public List<Integer> getAllMaterialCodes();

	@Query(value = "select et.tu_number from EtTransrate as et where et.werks in(:plantIDList) and et.kunnr in(:customrByPlant)")
	public List<String> getTuNumberByPlant(@Param("customrByPlant") List<String> customrByPlant,
			@Param("plantIDList") List<String> plantIDList);

	@Query(value = "select et.werks from EtTransrate as et left join MaterialTable as mt on "
			+ "CAST(et.werks AS string) = mt.plant")
	public List<Integer> getPlantBywerks();

	@Query(value = "select SUBSTRING(et.kunnr, 3) from EtTransrate as et where et.kunnr LIKE 'ZC%'")
	public List<String> getCustomersStartsWithZC();

	@Query(value = "select et.werks from EtTransrate as et"
			+ " where CAST(et.werks AS string) in (select mt.plant from MaterialTable as mt)")
	public List<String> getAllPlant();

	@Query(value = "select ett.kunnr from EtTransrate as ett where ett.werks in (:plantlist)")
	public List<String> getCustomerByPlant(@Param("plantlist") List<String> plantIDList);

//	@Query(value = "select rr.capcluster from RatesOReport as rr where et. in(:plantIDList) and et.kunnr in(:customrByPlant)")
//	public List<String> getCapClusterByPlant(@Param("customrByPlant") List<String> customrByPlant,
//			@Param("plantIDList") List<String> plantIDList);
	
	@Query(value = "select rr.capcluster from RatesOReport as rr")
	public List<String> getCapClusterByPlant();



}
