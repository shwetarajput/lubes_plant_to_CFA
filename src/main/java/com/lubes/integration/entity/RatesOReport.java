package com.lubes.integration.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "yv_contract_rates_o_report", schema = "pricing_tool")
public class RatesOReport {
	@Id
	private Integer id;
	private String bukrs;
	private String capcluster;
	private String cap_desc;
	private String destcluster;
	private String dest_desc;
	private String final_uom;
	private String final_uomkm;
	private String kunwe;
	private String matkl;
	private String name1;
	private String name2;
	private String rateapp_desc;
	private String remarks;
	private String rtdcluster;
	private String rtd_desc;
	private String tplst;
	private String vstel;
	private String yytenderno;
	private Date createddate;
	private boolean is_active;
	private Date modifieddate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBukrs() {
		return bukrs;
	}

	public void setBukrs(String bukrs) {
		this.bukrs = bukrs;
	}

	public String getCapcluster() {
		return capcluster;
	}

	public void setCapcluster(String capcluster) {
		this.capcluster = capcluster;
	}

	public String getCap_desc() {
		return cap_desc;
	}

	public void setCap_desc(String cap_desc) {
		this.cap_desc = cap_desc;
	}

	public String getDestcluster() {
		return destcluster;
	}

	public void setDestcluster(String destcluster) {
		this.destcluster = destcluster;
	}

	public String getDest_desc() {
		return dest_desc;
	}

	public void setDest_desc(String dest_desc) {
		this.dest_desc = dest_desc;
	}

	public String getFinal_uom() {
		return final_uom;
	}

	public void setFinal_uom(String final_uom) {
		this.final_uom = final_uom;
	}

	public String getFinal_uomkm() {
		return final_uomkm;
	}

	public void setFinal_uomkm(String final_uomkm) {
		this.final_uomkm = final_uomkm;
	}

	public String getKunwe() {
		return kunwe;
	}

	public void setKunwe(String kunwe) {
		this.kunwe = kunwe;
	}

	public String getMatkl() {
		return matkl;
	}

	public void setMatkl(String matkl) {
		this.matkl = matkl;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getRateapp_desc() {
		return rateapp_desc;
	}

	public void setRateapp_desc(String rateapp_desc) {
		this.rateapp_desc = rateapp_desc;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRtdcluster() {
		return rtdcluster;
	}

	public void setRtdcluster(String rtdcluster) {
		this.rtdcluster = rtdcluster;
	}

	public String getRtd_desc() {
		return rtd_desc;
	}

	public void setRtd_desc(String rtd_desc) {
		this.rtd_desc = rtd_desc;
	}

	public String getTplst() {
		return tplst;
	}

	public void setTplst(String tplst) {
		this.tplst = tplst;
	}

	public String getVstel() {
		return vstel;
	}

	public void setVstel(String vstel) {
		this.vstel = vstel;
	}

	public String getYytenderno() {
		return yytenderno;
	}

	public void setYytenderno(String yytenderno) {
		this.yytenderno = yytenderno;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public boolean isIs_active() {
		return is_active;
	}

	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}

	public Date getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}
