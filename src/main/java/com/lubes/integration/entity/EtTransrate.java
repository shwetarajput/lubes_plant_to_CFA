package com.lubes.integration.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "yv_lu_pck_rate_et_transrate", schema = "pricing_tool")
public class EtTransrate {

	@Id
	private Integer id;
	private Date begda;
	private String bzirk;
	private Date endda;
	private String kunnr;
	private Integer mandt;
	private String provisional;
	private String remarks;
	private String tu_number;
	private String veh_type;
	private Integer werks;
	private String yybase_rate;
	private Integer yyrate_applic;
	private String createddate;
	private String is_active;
	private String modifieddate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getBegda() {
		return begda;
	}

	public void setBegda(Date begda) {
		this.begda = begda;
	}

	public String getBzirk() {
		return bzirk;
	}

	public void setBzirk(String bzirk) {
		this.bzirk = bzirk;
	}

	public Date getEndda() {
		return endda;
	}

	public void setEndda(Date endda) {
		this.endda = endda;
	}

	public String getKunnr() {
		return kunnr;
	}

	public void setKunnr(String kunnr) {
		this.kunnr = kunnr;
	}

	public Integer getMandt() {
		return mandt;
	}

	public void setMandt(Integer mandt) {
		this.mandt = mandt;
	}

	public String getProvisional() {
		return provisional;
	}

	public void setProvisional(String provisional) {
		this.provisional = provisional;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTu_number() {
		return tu_number;
	}

	public void setTu_number(String tu_number) {
		this.tu_number = tu_number;
	}

	public String getVeh_type() {
		return veh_type;
	}

	public void setVeh_type(String veh_type) {
		this.veh_type = veh_type;
	}

	public Integer getWerks() {
		return werks;
	}

	public void setWerks(Integer werks) {
		this.werks = werks;
	}

	public String getYybase_rate() {
		return yybase_rate;
	}

	public void setYybase_rate(String yybase_rate) {
		this.yybase_rate = yybase_rate;
	}

	public Integer getYyrate_applic() {
		return yyrate_applic;
	}

	public void setYyrate_applic(Integer yyrate_applic) {
		this.yyrate_applic = yyrate_applic;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public String getIs_active() {
		return is_active;
	}

	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

}
