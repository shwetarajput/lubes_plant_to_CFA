package com.lubes.integration.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ZST_LU_PLANT_MATTYP")
public class MaterialTable {
	@Id
	@Column(name = "material")
	private Integer material_code;
	@Column(name = "material_type")
	private String material_type;
	private String plant;
	private Integer cfa;

	public Integer getMaterial_code() {
		return material_code;
	}

	public void setMaterial_code(Integer material_code) {
		this.material_code = material_code;
	}

	public String getMaterial_type() {
		return material_type;
	}

	public void setMaterial_type(String material_type) {
		this.material_type = material_type;
	}

	public String getPlant() {
		return plant;
	}

	public void setPlant(String plant) {
		this.plant = plant;
	}

	public Integer getCfa() {
		return cfa;
	}

	public void setCfa(Integer cfa) {
		this.cfa = cfa;
	}

}
