package com.lubes.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartradzConsumerLubesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartradzConsumerLubesApplication.class, args);

	}

}
