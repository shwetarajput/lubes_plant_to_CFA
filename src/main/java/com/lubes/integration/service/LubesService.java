package com.lubes.integration.service;

import java.util.List;

import com.lubes.integration.entity.EtTransrate;

public interface LubesService {
	// public List<EtTransrate> getAllTransrate(String startDate, String endDate);
	public List<EtTransrate> getAllTransrate();

	public List<EtTransrate> processDataForPlant2CFA();

//	public List<EtTransrate> getCustomer(String startDate, String endDate);

//	public List<SalesItem> fetchCustomerSalesArea(String salesStartDate, String salesEndDate,
//			String customercode);
//
//	List<CustomerHierarchy> getCustomerSalesPartner(String customersalesStartDate, String customersalesEndDate,
//			String customercode);

}
