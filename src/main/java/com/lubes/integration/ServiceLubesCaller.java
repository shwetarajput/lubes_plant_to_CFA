package com.lubes.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.lubes.integration.serviceImpl.LubesServiceImpl;

@Component
public class ServiceLubesCaller implements ApplicationListener<ApplicationReadyEvent> {
	@Autowired
	LubesServiceImpl lubesServiceImpl;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		// TODO Auto-generated method stub
		lubesServiceImpl.getAllTransrate();
	}

}
