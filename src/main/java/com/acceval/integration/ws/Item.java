
package com.acceval.integration.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for item complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="item"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="conditionFields" type="{http://ws.integration.acceval.com/}conditionField" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="conditionValues" type="{http://ws.integration.acceval.com/}conditionValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="validFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="validTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "item", propOrder = {
    "code",
    "conditionFields",
    "conditionValues",
    "validFrom",
    "validTo"
})
public class Item {

    protected String code;
    @XmlElement(nillable = true)
    protected List<ConditionField> conditionFields;
    @XmlElement(nillable = true)
    protected List<ConditionValue> conditionValues;
    @XmlElementRef(name = "validFrom", type = JAXBElement.class, required = false)
    protected JAXBElement<String> validFrom;
    @XmlElementRef(name = "validTo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> validTo;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the conditionFields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditionFields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionField }
     * 
     * 
     */
    public List<ConditionField> getConditionFields() {
        if (conditionFields == null) {
            conditionFields = new ArrayList<ConditionField>();
        }
        return this.conditionFields;
    }

    /**
     * Gets the value of the conditionValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditionValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionValue }
     * 
     * 
     */
    public List<ConditionValue> getConditionValues() {
        if (conditionValues == null) {
            conditionValues = new ArrayList<ConditionValue>();
        }
        return this.conditionValues;
    }

    /**
     * Gets the value of the validFrom property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getValidFrom() {
        return validFrom;
    }

    /**
     * Sets the value of the validFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setValidFrom(JAXBElement<String> value) {
        this.validFrom = value;
    }

    /**
     * Gets the value of the validTo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getValidTo() {
        return validTo;
    }

    /**
     * Sets the value of the validTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setValidTo(JAXBElement<String> value) {
        this.validTo = value;
    }

}
