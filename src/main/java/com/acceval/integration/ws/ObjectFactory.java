
package com.acceval.integration.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.acceval.integration.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://ws.integration.acceval.com/", "Exception");
    private final static QName _SendConditionRecords_QNAME = new QName("http://ws.integration.acceval.com/", "sendConditionRecords");
    private final static QName _SendConditionRecordsResponse_QNAME = new QName("http://ws.integration.acceval.com/", "sendConditionRecordsResponse");
    private final static QName _ConditionValueCurrency_QNAME = new QName("", "currency");
    private final static QName _ConditionValueUom_QNAME = new QName("", "uom");
    private final static QName _ItemValidFrom_QNAME = new QName("", "validFrom");
    private final static QName _ItemValidTo_QNAME = new QName("", "validTo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.acceval.integration.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link SendConditionRecords }
     * 
     */
    public SendConditionRecords createSendConditionRecords() {
        return new SendConditionRecords();
    }

    /**
     * Create an instance of {@link SendConditionRecordsResponse }
     * 
     */
    public SendConditionRecordsResponse createSendConditionRecordsResponse() {
        return new SendConditionRecordsResponse();
    }

    /**
     * Create an instance of {@link ConditionRecordsInMessage }
     * 
     */
    public ConditionRecordsInMessage createConditionRecordsInMessage() {
        return new ConditionRecordsInMessage();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link ConditionField }
     * 
     */
    public ConditionField createConditionField() {
        return new ConditionField();
    }

    /**
     * Create an instance of {@link ConditionValue }
     * 
     */
    public ConditionValue createConditionValue() {
        return new ConditionValue();
    }

    /**
     * Create an instance of {@link WebServiceStatusMessage }
     * 
     */
    public WebServiceStatusMessage createWebServiceStatusMessage() {
        return new WebServiceStatusMessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.integration.acceval.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendConditionRecords }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendConditionRecords }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.integration.acceval.com/", name = "sendConditionRecords")
    public JAXBElement<SendConditionRecords> createSendConditionRecords(SendConditionRecords value) {
        return new JAXBElement<SendConditionRecords>(_SendConditionRecords_QNAME, SendConditionRecords.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendConditionRecordsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendConditionRecordsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.integration.acceval.com/", name = "sendConditionRecordsResponse")
    public JAXBElement<SendConditionRecordsResponse> createSendConditionRecordsResponse(SendConditionRecordsResponse value) {
        return new JAXBElement<SendConditionRecordsResponse>(_SendConditionRecordsResponse_QNAME, SendConditionRecordsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "currency", scope = ConditionValue.class)
    public JAXBElement<String> createConditionValueCurrency(String value) {
        return new JAXBElement<String>(_ConditionValueCurrency_QNAME, String.class, ConditionValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "uom", scope = ConditionValue.class)
    public JAXBElement<String> createConditionValueUom(String value) {
        return new JAXBElement<String>(_ConditionValueUom_QNAME, String.class, ConditionValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "validFrom", scope = Item.class)
    public JAXBElement<String> createItemValidFrom(String value) {
        return new JAXBElement<String>(_ItemValidFrom_QNAME, String.class, Item.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "validTo", scope = Item.class)
    public JAXBElement<String> createItemValidTo(String value) {
        return new JAXBElement<String>(_ItemValidTo_QNAME, String.class, Item.class, value);
    }

}
